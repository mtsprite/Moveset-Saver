const fs = require("fs");
const Moveset = require("./Moveset").Moveset;

class MasterList{

    constructor(listPath){
        this.listPath = listPath;
        
        var listData = JSON.parse(fs.readFileSync(this.listPath, "utf-8"));
        this.list = { movesets: [] };

        for(var i = 0; i < listData.movesets.length; i++){
            this.list.movesets.push(new Moveset(
                listData.movesets[i].name, 
                listData.movesets[i].ability,
                listData.movesets[i].item,
                listData.movesets[i].nature,
                listData.movesets[i].moves,
                listData.movesets[i].gender,
                listData.movesets[i].isShiny,
                listData.movesets[i].evs,
                listData.movesets[i].ivs                                
            ));
        }

        //console.log(this.list);
    }

    write(){
        fs.writeFileSync(this.listPath, JSON.stringify(this.list, null, 4));
    }

    exportJSON(){
        return JSON.stringify(this.list, null, 4);
    }

    exportSD(){
        var listSD = [];
        for(var i = 0; i < this.list.movesets.length; i++){
            listSD.push(this.list.movesets[i].exportSD());
        }

        return listSD;
    }

    refresh(){
        var listData = JSON.parse(fs.readFileSync(this.listPath, "utf-8"));
        this.list = { movesets: [] };

        for(var i = 0; i < listData.movesets.length; i++){
            this.list.movesets.push(new Moveset(
                listData.movesets[i].name, 
                listData.movesets[i].ability,
                listData.movesets[i].item,
                listData.movesets[i].nature,
                listData.movesets[i].moves,
                listData.movesets[i].gender,
                listData.movesets[i].isShiny,
                listData.movesets[i].evs,
                listData.movesets[i].ivs                                
            ));
        }
    }

    add(moveset){
        this.list.movesets.push(new Moveset(
            moveset.name, 
            moveset.ability,
            moveset.item,
            moveset.nature,
            moveset.moves,
            moveset.gender,
            moveset.isShiny,
            moveset.evs,
            moveset.ivs                                
        ));
        //console.log(this.list.movesets);
        this.write();
    }

}

module.exports = {
    MasterList: MasterList
}