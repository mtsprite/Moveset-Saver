class Moveset {

    constructor(name, ability, item, nature, moves, gender, isShiny, evs, ivs){

        this.name = name;
        this.ability = ability;
        this.item = item;
        this.nature = nature;
        this.moves = moves;
        this.gender = gender;
        this.isShiny = isShiny;
        this.evs = evs;
        this.ivs = ivs;

    }

    exportJSON() {
        return JSON.stringify(this, null, 4);
    }

    exportSD(){

        var evs = "";
        for(i = 0; i < 6; i++){
            if(this.evs[i] != 0){
                evs += this.evs[i];
                switch(i){
                    case 0:
                         evs += " HP / ";
                        break;
                    case 1:
                        evs += " Atk / ";
                        break;
                    case 2:
                        evs += " Def / ";
                        break;
                    case 3:
                        evs += " SpA / ";
                        break;                                                
                    case 4:
                        evs += " SpD / ";
                        break;
                    case 5:
                        evs += " Spe / ";
                        break;
                }
            }
        }
        evs = evs.slice(0, -2);

        var moves = "";
        for(var i = 0; i < 4; i++){
            if(i == 3){
                moves += "- " + this.moves[i];
            }
            else {
                moves += "- " + this.moves[i] + "\n";
            }
        }

        var gender; 
        if(this.gender == "Male" || this.gender == "Female"){
            gender = ` (${this.gender.charAt(0)})`;
        }
        else{
            gender = "";
        }

        var showdownStr = 
            this.name + gender + " @ " + this.item + "\n" +
            "Ability: " + this.ability + "\n" +
            "Shiny: " + this.isShiny + "\n" +
            evs + "\n" + 
            this.nature + " Nature" + "\n" +
            moves;

        return showdownStr;
    }

}

module.exports = {
    Moveset: Moveset
}