# Moveset Saver

A simple application that lets you save your movesets for Pokemon.

## Features
- Have all youre movesets in one place.
- Easily edit and remove movesets.
- Import/Export to and from Pokemon Showdown.