$(document).ready(function() {

    const electron = require("electron");
    const {ipcRenderer} = electron;

    $("#submitBtn").click( (event) => {

        event.preventDefault();

        var evs = [
            $("#evHP").val(),
            $("#evAtk").val(),
            $("#evDef").val(),
            $("#evSpA").val(),
            $("#evSpD").val(),
            $("#evSpe").val()
        ];

        var ivs = [
            $("#ivHP").val(),
            $("#ivAtk").val(),
            $("#ivDef").val(),
            $("#ivSpA").val(),
            $("#ivSpD").val(),
            $("#ivSpe").val()
        ];

        var moves = [
            $("#moveOne").val(),
            $("#moveTwo").val(),
            $("#moveThree").val(),
            $("#moveFour").val()
        ]

        var moveset = {
            name: $("#name").val(),
            ability: $("#ability").val(),
            item: $("#item").val(),
            nature: $("#nature").val(),
            moves: moves,
            gender: $("#gender").val(),
            isShiny: $("#shiny").val(),
            evs: evs,
            ivs: ivs
        };

        console.log(moveset);
        ipcRenderer.send("addNew:add", moveset);
    });

});